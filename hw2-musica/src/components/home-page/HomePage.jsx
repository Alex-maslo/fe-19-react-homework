import React, { Component } from "react";
import "./HomePage.scss";
import ProductList from "../product-list/ProductList";
import Header from "../header/Header";

class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cartCount: 0,
      favoriteCount: 0,
    };
  }

  componentDidMount() {
    const favoriteItems = Object.entries(localStorage).filter(
      ([key, value]) => value === "true"
    );
    const favoriteCount = favoriteItems.length;
    this.setState({ favoriteCount });
  }

  handleStorageChange = () => {
    const favoriteItems = Object.values(localStorage).filter(
      (value) => value === "true"
    );
    const favoriteCount = favoriteItems.length;
    this.setState({ favoriteCount });
  };

  addToCart = () => {
    this.setState((prevState) => ({
      cartCount: prevState.cartCount + 1,
    }));
  };

  render() {
    const { cartCount, favoriteCount } = this.state;

    return (
      <>
        <Header cartCount={cartCount} favoriteCount={favoriteCount} />
        <div className="home-page__container">
          <ProductList
            updateFavoriteCount={this.handleStorageChange}
            addToCart={this.addToCart}
          />
        </div>
      </>
    );
  }
}

export default HomePage;
