import React, { Component } from "react";
import "./ProductCard.scss";
import Button from "../button/Button";

class ProductCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFavorite: localStorage.getItem(props.article) === "true",
    };
  }

  handleToggleFavorite = () => {
    this.setState((prevState) => {
      const newIsFavorite = !prevState.isFavorite;
      localStorage.setItem(this.props.article, newIsFavorite);
      this.props.updateFavoriteCount();
      return {
        isFavorite: newIsFavorite,
      };
    });
  };

  render() {
    const { name, price, image, article, color, model } = this.props;
    const { isFavorite } = this.state;

    return (
      <div className="product-card">
        <div className="product-card__header">
          <h2 className="product-card__name">{name}</h2>
          <h3 className="product-card__model">{model}</h3>
        </div>
        <img className="product-card__image" src={image} alt="" />
        <p className="product-card__article">Артикул: {article}</p>
        <p className="product-card__color">Колір: {color}</p>
        <p className="product-card__price">Ціна: {price} UAH </p>

        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <button
            className={`product-card__favorite ${isFavorite ? "favorite" : ""}`}
            onClick={this.handleToggleFavorite}
          >
            &#9733;
          </button>
          <Button textButton={"Додати в кошик"} />
        </div>
      </div>
    );
  }
}

export default ProductCard;
