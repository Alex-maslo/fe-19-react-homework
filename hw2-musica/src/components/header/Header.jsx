import React, { Component } from "react";
import "./Header.scss";

class Header extends Component {
  render() {
    const { cartCount, favoriteCount } = this.props;

    return (
      <header className="header">
        <div className="header__logo">Риболовля</div>
        <div className="header__icons">
          <div className="header__cart">
            <span className="header__cart-icon">&#128722;</span>
            <span className="header__cart-count">{cartCount}</span>
          </div>
          <div className="header__favorite">
            <span className="header__favorite-icon">&#10084;</span>
            <span className="header__favorite-count">{favoriteCount}</span>
          </div>
        </div>
      </header>
    );
  }
}

export default Header;
