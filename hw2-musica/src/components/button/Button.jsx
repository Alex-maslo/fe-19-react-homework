import React, { Component } from "react";
import "./Button.scss";
import Modal from "../modal/Modal";

export default class Button extends Component {
  constructor(props) {
    super(props);
    this.state = { isClicked: false };
  }

  buttonClicked = () => {
    this.setState({ isClicked: true });
  };

  closeModal = () => {
    this.setState({ isClicked: false });
  };

  render() {
    return (
      <>
        <button onClick={this.buttonClicked} className="button">
          {this.props.textButton}
        </button>
        {this.state.isClicked && (
          <Modal
            onClose={this.closeModal}
            header="Modal Header"
            text="Modal Text"
            footer="Modal Footer"
          />
        )}
      </>
    );
  }
}
