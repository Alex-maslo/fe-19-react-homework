import { Component } from "react";
import "./modal.scss";

class Modal extends Component {
  render() {
    const { header, text, footer, actions, isOpen, onClose, colorModal } =
      this.props;
    console.log(typeof colorModal);
    return (
      <div
        className="modal__wrapper"
        style={{ display: isOpen ? "block" : "none" }}
        onClick={onClose}
      >
        <div
          className="modal"
          style={colorModal}
          onClick={(e) => e.stopPropagation()}
        >
          <div className="modal__close-button" onClick={onClose}>
            &times;
          </div>
          <div className="modal__header">{header} </div>
          <p className="modal__text">{text}</p>{" "}
          <p className="modal__text">{footer}</p>
          <div className="modal__button-container">{actions}</div>
        </div>
      </div>
    );
  }
}

export default Modal;
