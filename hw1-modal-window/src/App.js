import { Component } from "react";
import Button from "./components/button/Button";
import Modal from "./components/modal/Modal";

const modalWindowDeclarations = [
  {
    id: "1",
    title: "Do you want to delete this file?",
    description:
      "Once you delete this file, it won't possible to undo this action.",
    footer: "Are you sure you want delete this file?",
  },
  {
    id: "2",
    title: "Ви бажаєте видалити цей файл?",
    description: "Після видалення цього файлу цю дію буде неможливо скасувати.",
    footer: "Ви впевнені, що хочете видалити цей файл?",
    styleModal: { backgroundColor: "blue" },
  },
];

const modalButtonStyles = {
  fontSize: "20px",
  backgroundColor: "rgba(0,0,0, .3)",
  color: "white",
  padding: "10px",
  borderRadius: "5px",
  border: "none",
  width: "140px",
  height: "50px",
  cursor: "pointer",
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false,
      modalTitle: "",
      modalDescription: "",
      modalFooter: "",
      modalStyle: {},
    };
  }

  handleModalOpen = (event) => {
    const id = event.target.id;
    const modalWindowData = modalWindowDeclarations.find(
      (modal) => modal.id === id
    );

    if (modalWindowData) {
      this.setState({
        isModalOpen: true,
        modalTitle: modalWindowData.title,
        modalDescription: modalWindowData.description,
        modalFooter: modalWindowData.footer,
        modalStyle: modalWindowData.styleModal,
      });
    }
  };

  handleModalClose = () => {
    this.setState({
      isModalOpen: false,
      modalTitle: "",
      modalDescription: "",
      modalFooter: "",
    });
  };

  render() {
    const modalActions = [
      <button key="button1" style={modalButtonStyles}>
        Ok
      </button>,
      <button key="button2" style={modalButtonStyles}>
        Cancel
      </button>,
    ];

    return (
      <div>
        <Modal
          isOpen={this.state.isModalOpen}
          actions={modalActions}
          colorModal={this.state.modalStyle}
          header={this.state.modalTitle}
          text={this.state.modalDescription}
          footer={this.state.modalFooter}
          onClose={this.handleModalClose}
        />

        <div className="container">
          <Button
            id="1"
            backgroundColor="red"
            text="Open first modal"
            onClick={this.handleModalOpen}
          />
          <Button
            id="2"
            backgroundColor="blue"
            text="Open second modal"
            onClick={this.handleModalOpen}
          />
        </div>
      </div>
    );
  }
}

export default App;
